var resources = angular.module('sneakpic.resources',['ActiveRecord']);
resources.factory('Question', ['ActiveRecord', function(ActiveRecord){
    return ActiveRecord.extend({
        $urlRoot: 'http://sneakpic.herokuapp.com/questions'
    });
}]);

resources.factory('Answer',['ActiveRecord', function(ActiveRecord){
    return ActiveRecord.extend({
        $name: 'answer',
        $urlRoot: 'http://sneakpic.herokuapp.com/answers'
    });
}]);

resources.factory('User',['ActiveRecord', function(ActiveRecord){
    return ActiveRecord.extend({
        $name: 'user',
        $urlRoot: 'http://sneakpic.herokuapp.com/users'
    });
}]);

resources.factory('Highscore',['ActiveRecord', function(ActiveRecord){
    return ActiveRecord.extend({
        $urlRoot: 'http://sneakpic.herokuapp.com/users/highscores'
    });
}]);
