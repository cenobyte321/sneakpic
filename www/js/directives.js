var directives = angular.module('sneakpic.directives',[]);

directives.directive('captionLetter', [function(){
    return {        
        scope: true,         
        restrict: 'A',        
        link: function(scope, element, attrs, controller) {

            scope.$on('character:selected', function(event, data){
                if(attrs.captionLetter == data.character) {
                    element.addClass('guessed');
                }
            });
        }
    };
}]);

directives.directive('selectOnClick',[function(){

    return {
        restrict: 'A',
        link: function(scope, element, attrs, controller) {
            element.bind('click', function(e){                
                element.attr('disabled', true);
            });
        }
    };
}]);