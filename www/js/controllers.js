var controllers = angular.module('sneakpic.controllers', []);

controllers.controller('BootloaderCtrl', ['$scope', 'Session', '$state', 'LoginCheck', function($scope, Session, $state, LoginCheck) {
    LoginCheck.checkBoot();
}]);

controllers.controller('NewUserCtrl', ['$scope', '$state', '$ionicPopup', 'User', 'Loader', 'Session', 'LoginCheck', function($scope, $state, $ionicPopup, User, Loader, Session, LoginCheck) {
    LoginCheck.check();
    
    $scope.user = new User({username: null});

    $scope.register = function() {
        Loader.loading();
        $scope.user.$save().then(function(data) {
            Session.put('user_id', data.id);
            $state.go('main'); 
        }). finally(Loader.loaded);

    };
}]);


controllers.controller('MainCtrl', ['LoginCheck', '$ionicViewService', function(LoginCheck, $ionicViewService) {
    $ionicViewService.clearHistory();
    LoginCheck.check();
}]);

controllers.controller('NewQuestionCtrl', ['$scope', 'Camera', '$ionicPopup', '$state', 'Uploader', 'Session', 'LoginCheck', 'Loader', function($scope, Camera, $ionicPopup, $state, Uploader, Session, LoginCheck, Loader) {
    LoginCheck.check();

    $scope.question = {picture: null, caption: null, user_id: Session.get('user_id')};

    $scope.capturePhoto = function() {
        Camera.takePicture().then(function(fileURI) {
            capturedPhoto(fileURI)
        }, function(message) {
            error(message)
        });
    };

    $scope.submitQuestion = function() {
        var confirmPopup = $ionicPopup.confirm({
            title: 'Subir foto',
            template: '¿Est&aacute;s seguro en subir el reto?',
            cancelText: 'Todav&iacute;a no',
            okText: 'Ya'
        }).then(function(answerInput) {
            if (answerInput) {
                Loader.loading();
                Uploader.upload(
                        'http://sneakpic.herokuapp.com/questions.json',
                        $scope.question.picture,
                        'question[picture]',
                        {
                            'question[caption]': $scope.question.caption,
                            'question[user_id]': $scope.question.user_id
                        })
                        .then(uploadSuccess, uploadError).finally(Loader.loaded);
            }
        });

    };

    function uploadSuccess(data) {
        $ionicPopup.alert({
            title: '¡Listo!',
            template: 'Se ha agregado la foto'
        }).then(function() {
            $state.go('main');
        });
    }

    function uploadError(error) {
        $ionicPopup.alert({
            title: 'Error',
            template: '¡Ups! Algo salió mal, intenta de nuevo'
        });
    }

    function capturedPhoto(file) {
        $scope.question.picture = file;
        $scope.$apply();
    }

    function error(message) {
        alert("Se ha producido un error al capturar la foto, intenta nuevamente.");
    }
}]);

controllers.controller('PlayCtrl', ['$scope', 'Question', '$ionicPopup', 'Loader', '$state', 'Answer', 'Session', 'LoginCheck', '$interval', function($scope, Question, $ionicPopup, Loader, $state, Answer, Session, LoginCheck, $interval){
    LoginCheck.check();

    $scope.question = {};
    $scope.lives = 3;
    $scope.totalSeconds = $scope.secondsLeft = 20000;
    $scope.secondsLeftPercentage = 100;

    var winningHash = {};
    var options = { params: {user_id: Session.get('user_id') } };
    var timerInterval = null;
    
    Loader.loading();
    Question.fetchOne('random', options).then(function(question){        
        $scope.question = question;
        $scope.question.caption_characters = question.caption.split('');        
        initWinningHash();        
        startTimer();
    }, function(data) {
        $ionicPopup.alert({
            title: 'Se acabaron las preguntas!',
            template: 'Se acabaron las preguntas :( ¡Regresa en un rato Sherlock!'
        }).then(function(){ $state.go('main') });
    }).finally(Loader.loaded);    

    $scope.selectCharacter = function(character) {
        $scope.$broadcast('character:selected',{character: character});
        evaluateAnswer(character);
    };

    function startTimer() {
        timerInterval = $interval(decreaseTime, 40);
    }

    function decreaseTime() {
        $scope.secondsLeft -= 40;
        $scope.secondsLeftPercentage = ($scope.secondsLeft / $scope.totalSeconds) * 100;
        if($scope.secondsLeft <= 0) {
            endGame(false, 'Game Over', '¡Se acabó el tiempo!');
        }
    }


    function initWinningHash() {
        angular.forEach($scope.question.caption_characters, function(character, key) {
            winningHash[character] = false;
        });
    }

    function evaluateAnswer(character) {
        if($scope.question.caption_characters.indexOf(character) == -1) {
            loseLife();
        }
        else {
            addCorrectAnswer(character);
        }
    }

    function addCorrectAnswer(character) {
        winningHash[character] = true;
        if (answerIsCorrect()) {
            endGame(true, 'Success!', 'Yeap');
        }
    }

    function endGame(answerIsCorrect, popUpTitle, popUpTemplate) {
        $interval.cancel(timerInterval);
        $ionicPopup.alert({
            title: popUpTitle,
            template: popUpTemplate
        }).then(function(){
            saveAnswer(answerIsCorrect);
        })
    }

    function saveAnswer(isCorrect) {
        var answer = new Answer({
            correct: isCorrect,
            user_id: Session.get('user_id'),
            question_id: $scope.question.id
        });

        Loader.loading();
        answer.$save().then(nextQuestion).finally(Loader.loaded);
    }

    function nextQuestion() {
        $state.transitionTo($state.current, {}, {
            reload: true,
            inherit: false,
            notify: true
        });
    }

    function answerIsCorrect() {
        for (character in winningHash) {
            if (!winningHash[character]) {
                return false;
            }
        }
        return true;
    }

    function loseLife() {
        --$scope.lives;
        if ($scope.lives == 0) {
            endGame(false, 'Game Over', 'Ya no te quedan vidas, intenta de nuevo');
        }
    }

}]);

controllers.controller('AnswersCtrl', ['LoginCheck', '$ionicViewService', 'Highscore', 'Loader', '$scope', function(LoginCheck, $ionicViewService, Highscore, Loader, $scope) {
    
    $scope.highscores = {};
    LoginCheck.check();
    Loader.loading();
    
    Highscore.fetchAll().then(function(data){ 
        $scope.highscores = data;
    }).finally(Loader.loaded);
}]);
