// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('sneakpic', ['ionic','sneakpic.controllers', 'sneakpic.services', 'sneakpic.resources', 'sneakpic.directives'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})


.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider
    .state('register', {
      url: '/register',
      templateUrl: 'views/users/new.html',
      controller: 'NewUserCtrl'
    })

    .state('main', {
      url: '/main',
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })

    .state('play',{
      url: '/play',
      templateUrl: 'views/play.html',
      controller: 'PlayCtrl'
    })
    
    .state('bootloader',{
        url: '/bootloader',
        templateUrl: 'views/users/new.html',
        controller: 'BootloaderCtrl'
    })

    .state('new_question',{
      url: '/questions/new',
      templateUrl: 'views/questions/new.html',
      controller: 'NewQuestionCtrl'
    })
    
    
    .state('answers',{
      url: '/answers/highscores',
      templateUrl: 'views/answers/highscores.html',
      controller: 'AnswersCtrl'
    });
    
    
    $urlRouterProvider.otherwise('/bootloader');
     
});