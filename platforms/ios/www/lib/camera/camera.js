var takePicture = function(scope) {
    navigator.camera.getPicture(
        function(fileURI) { cameraSuccess(scope, fileURI); },
        function(message) { cameraError(message); }, 
        {
            quality: 50,
            destinationType: Camera.DestinationType.fileURI,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 640,
            targetHeight: 960,
            correctOrientation: true
        }
    );
};

var cameraSuccess = function(scope, fileURI) {
    console.log("SUCCESS");
};

var cameraError = function(message) {
    console.log('Camera Error: ' + message);
};