var controllers = angular.module('sneakpic.controllers', []);

controllers.controller('NewUserCtrl', ['$scope', '$state', '$ionicPopup', function($scope, $state, $ionicPopup){
    $scope.user = {};

    $scope.register = function() {
        $state.go('main');
    };      
}]);

controllers.controller('MainCtrl', ['$scope', '$state', function($scope, $state){
    
}]);

controllers.controller('NewQuestionCtrl', ['$scope', function($scope){
    $scope.capturePhoto = function(){
        takePicture($scope);
    }
}]);

controllers.controller('PlayCtrl', ['$scope', function($scope){
    
}]);
