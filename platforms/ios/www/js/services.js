var services = angular.module('sneakpic.services', []);

// Show a loding spinner
services.service('Loader', ['$ionicLoading', function($ionicLoading){
    this.loading = function(template) {
        template = template || '<i class="ion-loading-c loading-icon"></i>';
        $ionicLoading.show({
            template: template
        });
    };

    this.loaded = function() {
        $ionicLoading.hide();
    };
}]);


// Upload files to the server
services.service('Uploader', ['$q', 'Loader', function($q, Loader){
    this.upload = function(url, fileURI, fileKey, data, headers) {
        var options = buildUploadOptions(fileURI, fileKey, data, headers);
        var fileTransfer = new FileTransfer();
        var deferred = $q.defer();
        fileTransfer.upload(
            fileURI, 
            encodeURI(url),
            function(result) { deferred.resolve(result); },
            function(error) { deferred.reject(error); },
            options
        );
        return deferred.promise;
    };    

    function buildUploadOptions(fileURI, fileKey, data, headers) {
        var options = new FileUploadOptions();
        options.fileKey = fileKey;
        options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);      
        options.mimeType = "image/jpeg";
        if(data) {
            options.params = data;
        }
        if(headers) {
            options.headers = headers;
        }
        return options;
    };
}]);