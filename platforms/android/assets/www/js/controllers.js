var controllers = angular.module('sneakpic.controllers', []);

controllers.controller('NewUserCtrl', ['$scope', '$state', '$ionicPopup', function($scope, $state, $ionicPopup) {
        $scope.user = {};

        $scope.register = function() {
            $state.go('main');
        };
    }]);

controllers.controller('MainCtrl', ['$scope', '$state', function($scope, $state) {

    }]);

controllers.controller('NewQuestionCtrl', ['$scope', 'Camera', '$ionicPopup', '$state', 'Uploader', function($scope, Camera, $ionicPopup, $state, Uploader) {

        $scope.question = {picture: null, caption: null, user_id: 1};

        $scope.capturePhoto = function() {
            Camera.takePicture().then(function(fileURI) {
                capturedPhoto(fileURI)
            }, function(message) {
                error(message)
            });
        };

        $scope.submitQuestion = function() {
            var informacion = "La información que se va a enviar al servidor es: \n";
            informacion += "Foto: " + $scope.image;
            informacion += "Palabra: " + $scope.caption;

            var confirmPopup = $ionicPopup.confirm({
                title: 'Subir foto',
                template: '¿Est&aacute;s seguro en subir el reto?',
                cancelText: 'Todav&iacute;a no',
                okText: 'Ya'
            }).then(function(answerInput) {
                //Upload de foto 
                if (answerInput) {
                    Uploader.upload('http://sneakpic.herokuapp.com/questions.json',
                            $scope.question.picture,
                            'question[picture]',
                            {
                                'question[caption]': $scope.question.caption,
                                'question[user_id]': $scope.question.user_id
                            }).then(function(correct) {
                                     alert("CORRECT: " + JSON.stringify(correct))
                                },
                            function(error) {
                                alert("ERROR: " + JSON.stringify(error))
                            });


                    var popUp = $ionicPopup.show({
                        title: 'Confirmacion',
                        template: 'Se ha agregado la foto',
                        buttons: [
                            {
                                text: '<strong>Ok</strong>',
                                type: 'button-positive'
                            }
                        ]
                    }).then(function(e) {
                        $state.go('main');
                    });
                }
            });

        };

        function capturedPhoto(file) {
            $scope.question.picture = file;
            $scope.$apply();
        }

        function error(message) {
            alert("Se ha producido un error al capturar la foto, intenta nuevamente.");
        }
    }]);

controllers.controller('PlayCtrl', ['$scope', function($scope) {

    }]);
