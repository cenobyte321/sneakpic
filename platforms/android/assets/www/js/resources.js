var resources = angular.module('sneakpic.resources',[]);
resources.factory('Question', ['ActiveRecord', function(ActiveRecord){
    return ActiveRecord.extend({
        $urlRoot: 'http://sneakpic.herokuapp.com/questions'
    });
}]);